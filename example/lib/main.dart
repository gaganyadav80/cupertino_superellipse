import 'package:flutter/material.dart';
import 'package:cupertino_superellipse/cupertino_superellipse.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Superellipse Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.grey[700],
        body: Align(
          alignment: Alignment.center,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Material(
                    clipBehavior: Clip.antiAlias,
                    borderRadius: BorderRadius.circular(24),
                    color: Colors.black12,
                    child: new Container(
                      height: 180,
                      width: 72,
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Container(
                            height: 120,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  new Container(
                    height: 36,
                  ),
                  new Text(
                    'BorderRadius',
                    style: TextStyle(fontSize: 18.0, color: Colors.white),
                  )
                ],
              ),
              new Container(
                width: 72,
              ),
              new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Material(
                    clipBehavior: Clip.antiAlias,
                    shape: SuperEllipse(n: 3),
                    color: Colors.black12,
                    child: new Container(
                      height: 180,
                      width: 72,
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          new Container(
                            height: 120,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  new Container(
                    height: 36,
                  ),
                  new Text(
                    'SuperEllipse',
                    style: TextStyle(fontSize: 18.0, color: Colors.green[400]),
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
