## 2.0.0

- Added null-safety support

## 1.0.1

- Updated README, formatted code, added CHANGELOG and example

## 1.0.0

- Initial release
